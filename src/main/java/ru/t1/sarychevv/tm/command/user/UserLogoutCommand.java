package ru.t1.sarychevv.tm.command.user;

public class UserLogoutCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "User logout.";
    }

    @Override
    public String getName() {
        return "logout";
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        serviceLocator.getAuthService().logout();
    }

}
