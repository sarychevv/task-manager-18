package ru.t1.sarychevv.tm.command.task;

public class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public void execute() {
        System.out.println("[TASKS CLEAR]");
        getTaskService().clear();
    }

}
