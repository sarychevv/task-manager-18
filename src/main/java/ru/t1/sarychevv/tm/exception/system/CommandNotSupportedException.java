package ru.t1.sarychevv.tm.exception.system;

import ru.t1.sarychevv.tm.exception.AbstractException;

public final class CommandNotSupportedException extends AbstractException {

    public CommandNotSupportedException() {
        super("Error! Command not supported...");
    }

    public CommandNotSupportedException(final String command) {
        super("Error! Command \"" + command + "\" not supported...");
    }

}
