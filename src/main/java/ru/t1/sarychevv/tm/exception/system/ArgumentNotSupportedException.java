package ru.t1.sarychevv.tm.exception.system;

public final class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! This argument not supported...");
    }

    public ArgumentNotSupportedException(final String argument) {
        super("Error! Argument \"" + argument + "\" not supported...");
    }

}
