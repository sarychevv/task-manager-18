package ru.t1.sarychevv.tm.api.service;

import ru.t1.sarychevv.tm.api.repository.IUserRepository;
import ru.t1.sarychevv.tm.enumerated.Role;
import ru.t1.sarychevv.tm.model.User;

public interface IUserService extends IUserRepository {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User removeByLogin(String login);

    User removeByEmail(String email);

    User removeById(String id);

    User setPassword(String id, String password);

    User updateUser(String id, String firstName, String lastName, String middleName);

}
