package ru.t1.sarychevv.tm.util;

import ru.t1.sarychevv.tm.exception.field.NumberIncorrectException;

import java.util.Scanner;

public class TerminalUtil {

    static Scanner SCANNER = new Scanner(System.in);

    public static String nextLine() {
        return SCANNER.nextLine();
    }

    public static Integer nextNumber() {
        final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (final RuntimeException e) {
            throw new NumberIncorrectException(value, e);
        }
    }

}
